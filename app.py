from flask import Flask
from flask_restful import Api
from flask_session import Session
from routes.auth import Auth
from routes.index import Index
from routes.redirector import Redirector
from routes.userurls import UserUrls
from routes.userurl import UserUrl
import settings

app = Flask(__name__)

# Setting up Session
app.config['SESSION_TYPE'] = 'filesystem'
app.config['SESSION_COOKIE_NAME'] = 'user'
app.config['SESSION_COOKIE_DOMAIN'] = settings.APP_HOST
Session(app)

# Setting up Routes
api = Api(app)
api.add_resource(Auth, '/signin')
api.add_resource(Index, '/')
api.add_resource(Redirector, '/<short_code>')
api.add_resource(UserUrls, '/users/<int:user_id>/urls')
api.add_resource(UserUrl, '/users/<int:user_id>/urls/<short_code>')

if __name__ == "__main__":
    app.run(host=settings.APP_HOST, port=settings.APP_PORT)
