-- Drop Procedures If Exist
DROP PROCEDURE IF EXISTS insertUrls;
DROP PROCEDURE IF EXISTS updateUrl;
DROP PROCEDURE IF EXISTS selectRedirectByShortUrl;
DROP PROCEDURE IF EXISTS selectShortUrlsByUserId;
DROP PROCEDURE IF EXISTS selectUrlsByUserId;
DROP PROCEDURE IF EXISTS updateClicksByShortUrl;
DROP PROCEDURE IF EXISTS deleteUrlByShortUrl;

-- Insert a new URL
DELIMITER //
CREATE PROCEDURE insertUrls(IN userIdIn INT, shortUrlIn VARCHAR(8), redirectIn VARCHAR(2083), clicksIn INT)
BEGIN
    INSERT INTO urls (userId, shortUrl, redirect, clicks) VALUES (userIdIn, shortUrlIn, redirectIn, clicksIn);
END //
DELIMITER ;

-- Update an existing URL
DELIMITER //
CREATE PROCEDURE updateUrl(IN shortUrlIn VARCHAR(8), redirectIn VARCHAR(2083))
BEGIN
    UPDATE urls SET redirect = redirectIn WHERE shortUrl LIKE shortUrlIn;
END //
DELIMITER ;

-- Find a redirect using a short URL
DELIMITER //
CREATE PROCEDURE selectRedirectByShortUrl(IN shortUrlIn VARCHAR(8))
BEGIN
    SELECT redirect FROM urls WHERE shortUrl LIKE shortUrlIn;
END //
DELIMITER ;

-- Get all short URLs for a specified user
DELIMITER //
CREATE PROCEDURE selectShortUrlsByUserId(IN userIdIn INT)
BEGIN
    SELECT shortUrl FROM urls WHERE userId = userIdIn;
END //
DELIMITER ;

-- Get all URLs for a specified user
DELIMITER //
CREATE PROCEDURE selectUrlsByUserId(IN userIdIn INT)
BEGIN
    SELECT urlId,shortUrl,redirect,clicks FROM urls WHERE userId = userIdIn;
END //
DELIMITER ;

-- Update the amount of clicks for a short URL
DELIMITER //
CREATE PROCEDURE updateClicksByShortUrl(IN shortUrlIn VARCHAR(8))
BEGIN
    UPDATE urls SET clicks = clicks + 1 WHERE shortUrl = shortUrlIn;
END //
DELIMITER ;

-- Delete a URL (by short URL)
DELIMITER //
CREATE PROCEDURE deleteUrlByShortUrl(IN shortUrlIn VARCHAR(8))
BEGIN
    DELETE FROM urls WHERE shortUrl = shortUrlIn;
END //
DELIMITER ;