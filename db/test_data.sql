DELETE FROM urls;
DELETE FROM users;

ALTER TABLE users AUTO_INCREMENT = 1;
ALTER TABLE urls AUTO_INCREMENT = 1;

INSERT INTO users (userName) VALUES ("Alice");
INSERT INTO users (userName) VALUES ("Bob");
INSERT INTO users (userName) VALUES ("Charlie");
INSERT INTO users (userName) VALUES ("Derek");
INSERT INTO users (userName) VALUES ("Eric");
INSERT INTO users (userName) VALUES ("Frank");

INSERT INTO urls (userId, shortUrl, redirect, clicks)
    VALUES (1, "80ENGG4X", "https://www.google.com/search?client=ubuntu&channel=fs&q=big+duck&ie=utf-8&oe=utf-8", 0);
INSERT INTO urls (userId, shortUrl, redirect, clicks)
    VALUES (1, "sSQT2iVb", "https://www.facebook.com", 0);
INSERT INTO urls (userId, shortUrl, redirect, clicks)
    VALUES (2, "Sn7Ld6tK", "https://gitlab.com/aidencurtis", 1);
INSERT INTO urls (userId, shortUrl, redirect, clicks)
    VALUES (3, "5ZGs87i6", "https://my.unb.ca/", 5);
INSERT INTO urls (userId, shortUrl, redirect, clicks)
    VALUES (5, "UhyC1VR1", "https://www.youtube.com/", 1);
INSERT INTO urls (userId, shortUrl, redirect, clicks)
    VALUES (5, "l4CAEZL9", "https://twitter.com/home", 13);
INSERT INTO urls (userId, shortUrl, redirect, clicks)
    VALUES (5, "kxanTki0", "https://en.wikipedia.org/wiki/Website#Static_website", 0);
INSERT INTO urls (userId, shortUrl, redirect, clicks)
    VALUES (6, "Wd3vqDNh", "https://xkcd.com/", 0);