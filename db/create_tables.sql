DROP TABLE IF EXISTS urls;
DROP TABLE IF EXISTS users;

CREATE TABLE users (
    userId      INT             NOT NULL    AUTO_INCREMENT,
    userName    VARCHAR(20)     NOT NULL,
    UNIQUE(userName),
    PRIMARY KEY (userId)
);

CREATE TABLE urls (
    urlId       INT             NOT NULL    AUTO_INCREMENT,
    userId      INT             NOT NULL,
    shortUrl    VARCHAR(8)      NOT NULL,
    redirect    VARCHAR(2083)   NOT NULL,
    clicks      INT             NOT NULL,
    PRIMARY KEY (urlId),
    FOREIGN KEY (userId) REFERENCES users(userId)
);