-- Drop Procedures If Exist
DROP PROCEDURE IF EXISTS insertUser;
DROP PROCEDURE IF EXISTS deleteUser;
DROP PROCEDURE IF EXISTS getUser;
DROP PROCEDURE IF EXISTS getUserById;

-- Insert a new user
DELIMITER //
CREATE PROCEDURE insertUser(IN userNameIn VARCHAR(20))
BEGIN
    INSERT INTO users (userName) VALUES (userNameIn);
END //
DELIMITER ;

-- Delete an existing user
DELIMITER //
CREATE PROCEDURE deleteUser(IN userIdIn VARCHAR(20))
BEGIN
    DELETE FROM users WHERE id = userIdIn;
END //
DELIMITER ;

-- Get an existing user by username
DELIMITER //
CREATE PROCEDURE getUser(IN userNameIn VARCHAR(20))
BEGIN
    SELECT * FROM users WHERE userName = userNameIn;
END //
DELIMITER ;

-- Get an existing user by id
DELIMITER //
CREATE PROCEDURE getUserById(IN userIdIn VARCHAR(20))
BEGIN
    SELECT * FROM users WHERE userId = userIdIn;
END //
DELIMITER ;