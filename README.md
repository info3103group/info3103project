# INFO3103 Project: URL Shortener

This project is a URL Shortener for INFO3103: Programming on the Web. It uses Python Flask and MariaDB.

## Setup

Set up the python virtual environment of your choice. Then, install the necessary packages through pip:

```bash
flask
flask-restful
python-dotenv
pymysql
```

Install MariaDB and set up a user to test with on your local environment. Currently, the app does not automatically create tables, so you will have to do that manually.

Finally, you need your .env file:
```env
DB_HOST=
DB_USER=
DB_PASS=
DB_IDENTIFIER=
```
Populate this file and place it in the root directory of the project along with app.py.
Finally, make sure to export the flask app.

```bash
export FLASK_APP=app.py
```

## Running the App

Ensure you have enabled your virtual environment and then run the following command:

```bash
flask run
```


## Reaching the Endpoints
To test an endpoint, you can GET or POST using curl. Any POST requests will use JSON data.
```bash
curl -X POST -H 'Content-Type: application/json' localhost:5000/newurl -d '{"url": "https://www.google.com/search?q=big+duck&client=chrome"}'
```
GET requests can be done through the browser or, again, via curl.
```bash
curl localhost:5000/TnENmyM8
```