import settings
import pymysql.cursors


class DBConnection:
    db_connection = None

    def __init__(self):
        self.db_connection = self.connect_db()

    def connect_db(self):
        return pymysql.connect(host=settings.DB_HOST,
                               user=settings.DB_USER,
                               password=settings.DB_PASS,
                               db=settings.DB_IDENTIFIER)

    def get_db(self):
        return self.db_connection

    def create_url_mapping(self, user_id, short_code, url):
        sql_proc = "insertUrls"
        try:
            with self.db_connection.cursor() as cursor:
                cursor.callproc(sql_proc, (user_id, short_code, url, 0))
                self.db_connection.commit()
        except Exception as e:
            print(e)

    def update_url_mapping(self, short_code, new_url):
        sql_proc = "updateUrl"
        try:
            with self.db_connection.cursor() as cursor:
                cursor.callproc(sql_proc, (short_code, new_url))
                self.db_connection.commit()
        except Exception as e:
            print(e)

    def get_url(self, short_code):
        sql_proc = "selectRedirectByShortUrl"
        try:
            with self.db_connection.cursor() as cursor:
                cursor.callproc(sql_proc, (short_code,))
                result = str(cursor.fetchone()[0])
                return result
        except Exception as e:
            print(e)

    def get_all_users_urls(self, user_id):
        sql_proc = "selectUrlsByUserId"
        try:
            with self.db_connection.cursor() as cursor:
                cursor.callproc(sql_proc, (user_id,))
                results = cursor.fetchall()

                urls = []
                keys = ['url_id', 'short_url', 'redirect', 'clicks']
                for entries in results:
                    url = (dict(zip(keys, entries)))
                    urls.append(url)

                return urls
        except Exception as e:
            print(e)

    def on_redirect(self, short_code):
        sql_proc = "updateClicksByShortUrl"
        try:
            with self.db_connection.cursor() as cursor:
                cursor.callproc(sql_proc, (short_code,))
                self.db_connection.commit()
        except Exception as e:
            print(e)

    def delete_shortened_url(self, short_code):
        sql_proc = "deleteUrlByShortUrl"
        try:
            with self.db_connection.cursor() as cursor:
                cursor.callproc(sql_proc, (short_code,))
                delete_successful = cursor.rowcount == 1
                self.db_connection.commit()
                return delete_successful
        except Exception as e:
            print(e)
        finally:
            self.db_connection.close()

    def register_user(self, username):
        sql_proc = "insertUser"
        try:
            with self.db_connection.cursor() as cursor:
                cursor.callproc(sql_proc, (username,))
                self.db_connection.commit()
        except Exception as e:
            print(e)

    def get_user(self, username):
        sql_proc = "getUser"
        try:
            with self.db_connection.cursor() as cursor:
                cursor.callproc(sql_proc, (username,))
                user_id = cursor.fetchone()[0]
                return user_id
        except Exception as e:
            print(e)

    def get_user_by_id(self, user_id):
        sql_proc = "getUserById"
        try:
            with self.db_connection.cursor() as cursor:
                cursor.callproc(sql_proc, (user_id,))
                user = str(cursor.fetchone())
                return user
        except Exception as e:
            print(e)

    def user_exists(self, username):
        sql_proc = "getUser"
        try:
            with self.db_connection.cursor() as cursor:
                cursor.callproc(sql_proc, (username,))
                user_exists = cursor.fetchone()
                return user_exists
        except Exception as e:
            print(e)
