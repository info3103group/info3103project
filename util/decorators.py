from flask import session, make_response
from functools import wraps
from db_connection import DBConnection


def logged_in(func):
    @wraps(func)
    def function_wrapper(*args, **kwargs):
        if 'username' not in session:
            response = 'User must be logged in.'
            response_code = 401
            return make_response(response, response_code)
        return func(*args, **kwargs)
    return function_wrapper


def is_own_user(func):
    @wraps(func)
    def function_wrapper(*args, **kwargs):
        db_connection = DBConnection()
        user_id = db_connection.get_user(session['username'])
        if kwargs['user_id'] != user_id:
            return make_response("You can only edit your own URLs!", 400)
        return func(*args, **kwargs)
    return function_wrapper
