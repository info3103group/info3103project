===== Executing Test Script =====
User: acurtis
Host: info3103.cs.unb.ca:29946
Cleared previous session file.
============= Login Bad Credentials =============
{"message": "The method is not allowed for the requested URL."}

============= Login Missing Password Field =============
{"message": "The method is not allowed for the requested URL."}

============= Login =============

Login successful!
=============================

============= New Url =============
Creating shortened URL for: https://github.com/
=============================

============= New Url With No Url Provided =============
{"message": "The browser (or proxy) sent a request that this server could not understand."}
=============================

============= Go To URL Redirect =============
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>Redirecting...</title>
<h1>Redirecting...</h1>
<p>You should be redirected automatically to target URL: <a href="https://github.com/">https://github.com/</a>.  If not click the link.=============================

============= Delete URL =============
Deleting short code: nLyvzuSn
=============================

============= Get URLs =============
Retrieving URLs for: 1
{
  "urls": [
    {
      "clicks": 1, 
      "redirect": "https://github.com/", 
      "short_url": "G41hJ4FA", 
      "url_id": 1
    }, 
    {
      "clicks": 1, 
      "redirect": "https://github.com/", 
      "short_url": "Ed8Shyrs", 
      "url_id": 2
    }
  ]
}
=============================

============= Get URLs Sorted By url_id =============
Retrieving URLs sorted by url_id for: 1
{
  "urls": [
    {
      "clicks": 1, 
      "redirect": "https://github.com/", 
      "short_url": "G41hJ4FA", 
      "url_id": 1
    }, 
    {
      "clicks": 1, 
      "redirect": "https://github.com/", 
      "short_url": "Ed8Shyrs", 
      "url_id": 2
    }
  ]
}
=============================

============= Get URLs Sorted By clicks Descending =============
Retrieving URLs sorted by clicks in descending order for: 1
{
  "urls": [
    {
      "clicks": 1, 
      "redirect": "https://github.com/", 
      "short_url": "G41hJ4FA", 
      "url_id": 1
    }, 
    {
      "clicks": 1, 
      "redirect": "https://github.com/", 
      "short_url": "Ed8Shyrs", 
      "url_id": 2
    }
  ]
}
=============================
