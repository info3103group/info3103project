from flask import make_response, request, abort, session, jsonify
from flask_restful import Resource
from ldap3 import Server, Connection
from ldap3.core.exceptions import LDAPException
from db_connection import DBConnection
import settings


class Auth(Resource):

    username = None
    password = None

    def post(self):
        if not request.json:
            abort(400)
        if 'username' not in request.json or 'password' not in request.json:
            abort(400)
        result = request.get_json()
        username = result['username']
        password = result['password']
        if username in session:
            response = {'status': 'User is already logged in.'}
            return make_response(jsonify(response), 200)
        else:
            try:
                ldap_server = Server(host=settings.LDAP_HOST)
                ldap_connection = Connection(
                    ldap_server,
                    raise_exceptions=True,
                    user=f'uid={username},ou=People,ou=fcs,o=unb',
                    password=password
                )
                ldap_connection.open()
                ldap_connection.start_tls()
                ldap_connection.bind()

                db_connection = DBConnection()
                user_exists = db_connection.user_exists(username)
                if not user_exists:
                    db_connection.register_user(username)
                session['username'] = username
                user_id = str(db_connection.get_user(username))
                response = {'user_url': request.url_root + 'users/' + user_id}
                response_code = 201
            except (LDAPException):
                response = {'status': 'Access Denied (Incorrect Credentials)'}
                response_code = 403
            finally:
                ldap_connection.unbind()
        return make_response(jsonify(response), response_code)

    def get(self):
        if 'username' in session:
            response = {'status': 'Success'}
            response_code = 200
        else:
            response = {'status': 'Failure'}
            response_code = 403
        return make_response(jsonify(response), response_code)
