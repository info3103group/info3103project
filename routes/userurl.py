from flask import make_response, abort, jsonify
from flask_restful import Resource, request
from util.decorators import logged_in, is_own_user
from db_connection import DBConnection


class UserUrl(Resource):

    @logged_in
    @is_own_user
    def put(self, user_id, short_code):
        result = request.get_json()
        if 'url' not in result:
            return abort(400)
        new_url = result['url']
        db_connection = DBConnection()
        url_exists = db_connection.get_url(short_code)

        if url_exists:
            db_connection.update_url_mapping(short_code, new_url)
            return make_response(jsonify(
                {"short_url": request.url_root + short_code}), 200)
        else:
            return make_response("Requested shortened URL not found", 404)

    @logged_in
    @is_own_user
    def delete(self, user_id, short_code):
        db_connection = DBConnection()
        delete_successful = db_connection.delete_shortened_url(short_code)

        if delete_successful:
            return make_response("", 204)
        else:
            return make_response("Requested shortened URL not found", 404)
