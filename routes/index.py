from flask import make_response
from flask_restful import Resource


class Index(Resource):
    def get(self):
        return make_response('Index', 200)
