from flask import make_response, jsonify, abort
from flask_restful import Resource, request, reqparse
from db_connection import DBConnection
from util.decorators import logged_in, is_own_user
import random
import string


class UserUrls(Resource):
    def get(self, user_id):
        db_connection = DBConnection()
        urls = db_connection.get_all_users_urls(user_id)

        parser = reqparse.RequestParser()
        parser.add_argument('sort', location='args', )
        args = parser.parse_args()

        if args['sort']:
            sort_params = args['sort']
            is_search_reversed = False

            if sort_params.startswith('-'):
                is_search_reversed = True
                sort_params = sort_params.replace('-', '')
            elif sort_params.startswith('+'):
                sort_params = sort_params.replace('+', '')

            if sort_params in urls[0]:
                urls = sorted(urls,
                              key=lambda k: k[sort_params],
                              reverse=is_search_reversed)
            else:
                abort(400)

        if urls:
            return make_response(jsonify({"urls": urls}), 200)
        else:
            return make_response("Requested user URLs not found", 404)

    @logged_in
    @is_own_user
    def post(self, user_id):
        if not request.json or 'url' not in request.json:
            abort(400)

        result = request.get_json()
        url = result['url']
        url = str(url)
        url = url.strip()

        if not url.startswith('https://') and not url.startswith('http://'):
            abort(400)

        chars = string.ascii_letters + string.digits
        short_code = ''.join(random.choice(chars) for ch in range(8))

        db_connection = DBConnection()
        db_connection.create_url_mapping(user_id, short_code, url)

        uri = request.url_root + short_code

        return make_response(jsonify({'uri': uri}), 201)
