from flask import make_response, redirect
from flask_restful import Resource
from db_connection import DBConnection


class Redirector(Resource):

    def get(self, short_code):
        db_connection = DBConnection()
        url = db_connection.get_url(short_code)
        if (url):
            db_connection.on_redirect(short_code)
            return redirect(url, 301)
        else:
            return make_response("Shortened URL not found.\n", 404)
