#!/bin/bash

# URL Shortener Test Suite
# Runs the test cases found at the bottom of file
# Pass the script 'debug' when executing to enable debug output
# Example: ./test_suite debug

echo "===== Executing Test Script ====="

if [ "$1" == "debug" ]; then
  echo "Debug enabled!"
  echo ""
  set -x
fi

# --- Test Session Variables ---
session_file="cookie-jar"
should_clear_session=1
user=$(whoami)
host="info3103.cs.unb.ca"
port="29946"
hostname=$host:$port

# --- Test Suite Variables ---
user_id=0
url_short_code=""

echo "User: $user"
echo "Host: $hostname"

if [ "$should_clear_session" == 1 ]; then
  if [ -f $session_file ]; then
    rm $session_file
    echo "Cleared previous session file."
  fi
fi


# Example:
# curl -X POST -H 'Content-Type: application/json' info3103.cs.unb.ca:29946/signin -d '{"username":"jandrade", "password":"hunter1"}' -c cookie-jar
login() {
  echo "============= Login ============="

  if [ -f $session_file ]; then
    echo "User already logged in. Skipping login."
    return
  fi

  route="signin"
  read -p "Username: " username
  read -s -p "Password: " password
  echo ""

  user_id=$(curl -X POST -H "Content-Type: application/json" "$hostname/$route" -d '{"username":"'$username'", "password":"'$password'"}' -c $session_file | cut -d':' -f4 | cut -d'/' -f3 | grep -o '[[:digit:]]*')

  if [ -f $session_file ]; then
    echo "Login successful!"
  else
    echo "Login unsuccesful. Exiting."
    exit 1
  fi

  echo -e "=============================\n"
}

# Example:
# curl -X POST -H 'Content-Type: application/json' info3103.cs.unb.ca:16346/ -d '{"username":"donaldtrump", "password":"makeamericagreatagain"}'
login_bad_credentials() {
  echo "============= Login Bad Credentials ============="
  username="donaldtrump"
  password="makeamericagreatagain"
  curl -X POST -H "Content-Type: application/json" "$hostname/$route" -d '{"username":"'$username'", "password":"'$password'"}'
  echo ""
}

# Example:
# curl -X POST -H 'Content-Type: application/json' info3103.cs.unb.ca:16346/ -d '{"username":"homersimpson"}'
login_missing_credentials() {
  echo "============= Login Missing Password Field ============="
  username="homersimpson"
  curl -X POST -H "Content-Type: application/json" "$hostname/$route" -d '{"username":"'$username'"}'
  echo ""
}

# Example:
# curl -X POST -H 'Content-Type: application/json' info3103.cs.unb.ca:29946/users/5/urls -d '{"url":"https://github.com/"}' -b cookie-jar
create_new_url() {
  echo "============= New Url ============="
  route="users/$user_id/urls"
  url="https://github.com/"
  echo "Creating shortened URL for: $url"
  url_short_code=$(curl -X POST -H "Content-Type: application/json" "$hostname/$route" -d '{"url":"'$url'"}' -b $session_file | tr '\n' ' ' | cut -d'/' -f4 | cut -d'"' -f1)
  echo -e "=============================\n"
}

# Example:
# curl -X POST -H 'Content-Type: application/json' info3103.cs.unb.ca:16346/users/7/urls -b cookie-jar
create_new_url_no_url_provided() {
  echo "============= New Url With No Url Provided ============="
  route="users/$user_id/urls"
  curl -X POST -H "Content-Type: application/json" "$hostname/$route" -b $session_file
  echo -e "=============================\n"
}

# Example:
# curl -X GET -H 'Content-Type: application/json' info3103.cs.unb.ca:16346/users/7/urls -b cookie-jar
get_users_urls() {
  echo "============= Get URLs ============="
  route="users/$user_id/urls"
  echo "Retrieving URLs for: $user_id"

  curl -X GET -H "Content-Type: application/json" "$hostname/$route" -b $session_file
  echo -e "=============================\n"
}

# Example:
# curl -X GET -H 'Content-Type: application/json' 'info3103.cs.unb.ca:29946/users/2/urls?sort=url_id' -b cookie-jar
get_users_urls_sorted_by_url_id() {
  echo "============= Get URLs Sorted By url_id ============="
  route="users/$user_id/urls"
  query="?sort=url_id"
  echo "Retrieving URLs sorted by url_id for: $user_id"

  curl -X GET -H "Content-Type: application/json" "$hostname/$route$query" -b $session_file
  echo -e "=============================\n"
}

# Example
# curl -X GET -H 'Content-Type: application/json' 'info3103.cs.unb.ca:29946/users/2/urls?sort=-clicks' -b cookie-jar
get_users_urls_sorted_by_clicks_descending() {
  echo "============= Get URLs Sorted By clicks Descending ============="
  route="users/$user_id/urls"
  query="?sort=-clicks"
  echo "Retrieving URLs sorted by clicks in descending order for: $user_id"

  curl -X GET -H "Content-Type: application/json" "$hostname/$route$query" -b $session_file
  echo -e "=============================\n"
}

# Example:
# curl -X DELETE -H 'Content-Type: application/json' info3103.cs.unb.ca:16346/users/7/urls/te7Is9ad -b cookie-jar
delete_url() {
  echo "============= Delete URL ============="
  route="users/$user_id/urls"
  echo "Deleting short code: $url_short_code"

  curl -X DELETE -H "Content-Type: application/json" "$hostname/$route/$url_short_code" -b $session_file
  echo -e "=============================\n"
}

# Example:
# curl -X GET -H 'Content-Type: application/json' info3103.cs.unb.ca:16346/te7Is9ad -b cookie-jar
go_to_url_redirect() {
  echo "============= Go To URL Redirect ============="
  curl -X GET -H "Content-Type: application/json" "$hostname/$url_short_code" -b $session_file
  echo -e "=============================\n"
}

# ----- Execute the tests ------
login_bad_credentials
login_missing_credentials
login
create_new_url
create_new_url_no_url_provided
go_to_url_redirect
delete_url
get_users_urls
get_users_urls_sorted_by_url_id
get_users_urls_sorted_by_clicks_descending
